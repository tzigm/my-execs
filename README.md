# TL;DR #
My repository, where I will be pushing some interesting scripts.
# Usefull links: #
* [C languages strings and how to work with them](https://www.tutorialspoint.com/cprogramming/c_strings.htm) 
* [How to use Windows' START batch function](https://ss64.com/nt/start.html)
* [.gitignore tooltips](https://www.atlassian.com/git/tutorials/gitignore)