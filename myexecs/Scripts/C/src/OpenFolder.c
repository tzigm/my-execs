#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Defines
//#define MODE "DBG" // DBG - debug; REL - release

// Global vars
char rFName[] = "cmdpth.names"; //resources file name

// Function declarations
void WaitForInput();
void SearchForCommand(char cmd[], char *rtrn[]);

// Code
int main(int argc, char *argv[]) {
  //printf("Hello world. I am working!\n");
  printf("argc = %i\nargv[0] = %s\nargv[1] = %s", argc, argv[0], argv[1]);

  char *rez[128];
  SearchForCommand(argv[0], rez);
  printf("%s", rez);

  WaitForInput();
  return 0;
}

// Returns path (if found)
void SearchForCommand(char cmd[], char *rtrn[]){
  FILE *fp;
  char buff[255];

//#if MODE == "DBG"
  char folder[] = "\\resources\\";
  strcat(folder, rFName);
  fp = fopen(folder, "r");
  fgets(buff, 128, fp);
  printf("%s", buff);
  WaitForInput();
//#else

//#endif
  *rtrn = buff;
}

void WaitForInput(){
  char temp[32];
  scanf("%s", temp);
}
