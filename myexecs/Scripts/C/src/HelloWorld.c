#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char *args[]) {
	printf("Hello world!");

	//exit(255); // this will exit application with errorlevel 255
	// After exit program terminates this script (doesn't go futher)
	// '%errorlevel%' is to see execute code

	char cmd[] = "start \"\" ";
	strcat(cmd, args[1]);
	// Note: args[0] - command name. can check with printf("%s", args[0]);

	printf("First data in array: %s", args[0]);
	printf("\n'argc' value = %i", argc);

	// Cool note: if you want to see results:
	// 1. execute this script directly through command line (and not .bat file)
	// or 2. use scanf() function. (commented below)
	
	//char temp[32];
	//scanf(temp);

	system(cmd);

	return 0; // if exit is not included errorlevel will be this return value	
}
