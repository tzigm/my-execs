package openf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class OpenFolder {

	static String dataFileName = "\\mycmd.pths";
	
	public static void main(String[] args) {
		if(args.length < 1){
			// If no parameters were given. Print how to use this command
			System.out.println("There are zero arguments. Opening Documents folder.");
			executeCommand("");
			System.exit(255);
			return; // no need for this.
		}
		
		// Lets check if user has written predefined commands
		// Possible predefined commands:
		// -help	- prints all possible commands
		// -list	- prints all possible defined to paths commands
		// -add		- adds new path with command (future)
		// All predefined commands should start with '-' symbol
		if(args[0].charAt(0) == '-'){
			switch(args[0]){
				case "-help":
					argHelp();
					break;
				case "-list":
					argList();
					break;
				case "-add":
					// TODO: Implement -add function
					break;
				// Default case returns that predefined command was not found.
				default:
					System.out.println("Command was not found.");
					argHelp();
					break;
			}
		}
		else{
			executeCommand(getComPath(args[0]));			
		}
	}
	
	private static String getComPath(String command){
		
		// System searches in project's directory. Also, you have to add 'dot' at the beginning of path
		try(BufferedReader br = new BufferedReader(new FileReader(getFilePath()))){
			String line = "";
			while((line = br.readLine()) != null){
				if(line.length() == 0) { continue; }
				if (line.charAt(0) == '#')	{
					continue;
				}
				String[] lineargs = line.split(":->:");
				if(lineargs.length != 2){
					throw new Exception("Bumped into not full or corrupted command!");
				}
				if(lineargs[0].compareToIgnoreCase(command) == 0){
					System.out.println("Command was found successfuly");
					return lineargs[1];
				}
			}
		}
		catch (FileNotFoundException fnfe){
			// Print what will happen when system wont be able to locate data file.
			System.err.println(fnfe.getMessage());
		}
		catch (IOException ioe){
			System.err.println(ioe.getMessage());
		}
		catch (Exception e){
			System.out.println(e.getMessage());
			return null;
		}
		return "-1:CMDNF";
	}
	
	private static String getFilePath(){
		String decodedPath = "";
		try {
			String dataFilePath = OpenFolder.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			decodedPath = URLDecoder.decode(dataFilePath, "UTF-8"); // We have to replace our %20, which are caused by spaces in path
		} 
		catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			return "FNF";
		} 
		return String.format("%s\\..\\resources\\%s", decodedPath, dataFileName);
	}

	private static void executeCommand(String path){
		if(path == "-1:CMDNF"){
			System.out.println("Command was not found.\nUse -list parameter to see available commands.");
			return;
		}
		
		String cmd = String.format("explorer \"%s\"", path);
		
		try {
			/*Process process = */Runtime.getRuntime().exec(cmd);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(255);
		}
	}
	
	private static void argHelp(){
		System.out.println("Welcome to open folder script help.\nTake a note that all predefined commands start with '-' symbol");
		System.out.println("Here are all possible argument commands:");
		System.out.println("-help\t- prints all possible commands.");
		System.out.println("-list\t- prints all possible defined paths and commands");
		System.out.println("-add\t- add new path with command");
	}

	private static void argList(){
		System.out.println("Here's list of all possible paths:");
		System.out.println("ID Command\tID path");
		try(BufferedReader br = new BufferedReader(new FileReader(getFilePath()))){
			String line = "";
			int i = 1;
			while((line = br.readLine()) != null){
				if(line.length() == 0) { continue; }
				
				if(line.charAt(0) != '#'){
					String[] lineargs = line.split(":->:");
					System.out.printf("%d %s\t\t%d %s\n", i, lineargs[0], i, lineargs[1]);
					i++;
				}
			}
		}
		catch (FileNotFoundException fnfe){
			// Print what will happen when system wont be able to locate data file.
			fnfe.printStackTrace();
		}
		catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

	private static void argAdd(String arg){
		
	}
}
