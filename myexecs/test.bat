@echo off

echo Hello from bat file
start "" /wait "%~dp0\Scripts\C\hw.exe" ".\.."
:: wait lets us pass errorlevel
echo %errorlevel% 
exit /b %errorlevel%
:: %~dp0 returns current bat file's path
:: more about this:
:: https://ss64.com/nt/start.html
:: start "" ""
