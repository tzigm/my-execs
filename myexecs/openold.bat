﻿@echo off
set fp_back="./.."
set fp_ktu="C:\Users\Tomas\Documents\_.KTU"

set cmd_back="back"
set cmd_ktu="ktu"
set cmd_current="current"

set cmd_help="--help"
set cmd_c="-c"

if "%1" == "" (
	goto args_failure
)

if "%1" == %cmd_help% (
	goto args_help
)

if "%1" == %cmd_c% (
	if "%2" == "" (
		goto args_failure_c
	)
	goto args_successful_c
)

goto args_successful

:args_successful
set change_to=""

if "%1"==%command0% (
	echo success back
	set change_to=folder_path0
	goto args_execute
)
if "%1"==%command_ktu% (
	echo success ktu
	set change_to=%folder_path_ktu%
	goto args_execute
)
exit /b 0

:args_failure
echo use: open [folder]
echo type: open --help to check for available folder commands
exit /b 1

:args_execute
echo starting %change_to%
start "" %change_to%
exit /b 0

:args_help
exit /b 0

:args_failure_c

exit /b 0

:args_successful_c
set change_to=""

if "%1"==%command0% (
	echo success back
	set change_to=%fp_back%
)
if "%1"==%command_ktu% (
	echo success ktu
	set change_to=%fp_ktu%
)
exit /b 0
